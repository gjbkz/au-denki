import * as fs from 'node:fs';
import * as path from 'node:path';
import * as readline from 'node:readline';

const run = async () => {
    {
        const dest = path.join(__dirname, 'data', 'formatted.csv');
        await fs.promises.writeFile(dest, '');
        for await (const line of generate(',')) {
            await fs.promises.appendFile(dest, `${line}\n`);
        }
    }
    {
        const dest = path.join(__dirname, 'data', 'formatted.tsv');
        await fs.promises.writeFile(dest, '');
        for await (const line of generate('\t')) {
            await fs.promises.appendFile(dest, `${line}\n`);
        }
    }
};

const generate = async function* (delimiter: string) {
    yield [...listHeaders()].join(delimiter);
    const directory = path.join(__dirname, 'data', 'raw');
    for (const name of await fs.promises.readdir(directory)) {
        const file = path.join(directory, name);
        const data: Array<string | number> = [name.split('.', 1)[0]];
        const rl = readline.createInterface(fs.createReadStream(file));
        let count = 0;
        for await (const line of rl) {
            switch (count) {
            case 0:
            case 3:
                break;
            case 1:
            case 2:
                data[count] = parseFloat(line);
                break;
            default:
                data[count - 1] = parseFloat(line.split(/\s+/)[1]);
            }
            count += 1;
        }
        yield data.join(delimiter);
    }
};

const listHeaders = function* () {
    yield '日付';
    yield '合計(kWh)';
    yield '料金(円)';
    for (let hour = 0; hour < 24; hour++) {
        const h = `${hour}`.padStart(2, '0');
        yield `${h}:00`;
        yield `${h}:30`;
    }
};

run().catch((error) => {
    console.error(error);
    process.exit(1);
});
