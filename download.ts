import * as path from 'node:path';
import * as fs from 'node:fs';
import {Builder, WebDriver, until, By} from 'selenium-webdriver';

const [,, auID, password] = process.argv;

let driver: WebDriver | undefined;

const run = async () => {
    driver = await new Builder().forBrowser('chrome').build();
    await login(driver);
    const dataDirectory = await setupDirectory();
    const maxCount = 370;
    for (let count = 1; count <= maxCount; count++) {
        const date = new Date();
        date.setDate(date.getDate() - count);
        console.info(`${date.toDateString()} (${count}/${maxCount})`);
        const dest = path.join(dataDirectory, `${date.toISOString().split('T')[0]}.txt`);
        {
            /** タイトルを読む */
            const element = await driver.findElement(By.css('#l-wrap > div > main > div > article > section > div.c-power-score > div > div.c-power-score__detail--right'));
            const title = await element.getText();
            await fs.promises.writeFile(dest, `${title}\n`);
            console.info(title);
        }
        {
            /** 詳細を開く */
            const query = By.css('#l-wrap > div > main > div > article > section > div.u-center > div');
            const element = await driver.findElement(query);
            await element.click();
        }
        {
            /** データを読む */
            const query = By.css('#l-wrap > div.c-modal-target > div > article > div > div > table');
            const element = await driver.findElement(query);
            const data = await element.getText();
            await fs.promises.appendFile(dest, data);
        }
        {
            /** 詳細を閉じる */
            const query = By.css('#l-wrap > div.c-modal-target > div > div > a');
            const element = await driver.findElement(query);
            await element.click();
        }
        {
            /** 前日へ移動する */
            const query = By.css('#l-wrap > div > main > div > article > div > div > a:nth-child(1)');
            const element = await driver.findElement(query);
            await element.click();
        }
        await wait(100);
    }
};

run()
.catch((error) => console.error(error))
.finally(async () => {
    if (driver) {
        await driver.quit();
    }
})
.catch((error) => {
    console.error(error);
    process.exit(1);
});

const wait = (durationMs: number) => new Promise((resolve) => setTimeout(resolve, durationMs));
const login = async (driver: WebDriver) => {
    const url = 'https://mieru.auone.jp/#/results/daily';
    await driver.get(url);
    {
        const query = By.id('loginAliasId');
        await driver.wait(until.elementsLocated(query));
        const element = await driver.findElement(query);
        await element.sendKeys(auID);
    }
    {
        const query = By.id('btn_idInput');
        const element = await driver.findElement(query);
        await element.click();
    }
    {
        const query = By.id('loginAuonePwd');
        await driver.wait(until.elementsLocated(query));
        const element = await driver.findElement(query);
        await element.sendKeys(password);
    }
    {
        const query = By.id('btn_pwdLogin');
        const element = await driver.findElement(query);
        await element.click();
    }
    await driver.wait(until.urlIs(url), 180000);
};
const setupDirectory = async () => {
    const dataDirectory = path.join(__dirname, 'data', 'raw');
    await fs.promises.mkdir(dataDirectory, {recursive: true});
    return dataDirectory;
};